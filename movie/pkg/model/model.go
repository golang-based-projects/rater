package model

import "github.com/ivanbulyk/cinemapp/metadata/pkg/model"

// MovieDetails includes movie metadata, its agregated rating
type MovieDetails struct {
	Rating   float64        `json:"rating,omitempty"`
	Metadata model.Metadata `json:"metadata"`
}
