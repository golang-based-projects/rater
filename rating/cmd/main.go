package main

import (
	"log"
	"net/http"

	"github.com/ivanbulyk/cinemapp/rating/internal/controller/rating"
	"github.com/ivanbulyk/cinemapp/rating/internal/handler/httphandler"
	"github.com/ivanbulyk/cinemapp/rating/internal/repository/memory"
)

func main() {
	log.Println("Statrting the rating service")
	repo := memory.New()
	ctrl := rating.New(repo)
	h := httphandler.New(ctrl)
	http.Handle("/rating", http.HandlerFunc(h.Handle))
	if err := http.ListenAndServe(":8082", nil); err != nil {
		panic(err)
	}
}
