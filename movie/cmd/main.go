package main

import (
	"log"
	"net/http"

	"github.com/ivanbulyk/cinemapp/movie/internal/controller/movie"
	"github.com/ivanbulyk/cinemapp/movie/internal/gateway/metadata/metadatahttpgateway"
	"github.com/ivanbulyk/cinemapp/movie/internal/gateway/rating/ratinghttpgateway"
	"github.com/ivanbulyk/cinemapp/movie/internal/handler/httphandler"
	// "github.com/ivanbulyk/cinemapp/movie/internal/gateway/rating/ratinghttpgateway"
	// "github.com/ivanbulyk/cinemapp/rating/internal/controller/rating"
)

func main() {
	log.Println("Starting the movie service")
	metadataGateway := metadatahttpgateway.New("localhost:8081")
	ratingGateway := ratinghttpgateway.New("localhost:8082")
	ctrl := movie.New(ratingGateway, metadataGateway)
	h := httphandler.New(ctrl)
	http.Handle("/movie", http.HandlerFunc(h.GetMovieDetails))
	if err := http.ListenAndServe(":8083", nil); err != nil {
		panic(err)
	}
}
